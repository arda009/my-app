package com.mycompany.app;

import java.util.Scanner;

public class App {
	public int add(int a, int b) {
		return a+b;
	}

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in); //System.in is a standard input stream.
		System.out.print("Enter First Number: ");
		int a = sc.nextInt();
		System.out.print("Enter Second Number: ");
		int b = sc.nextInt();
		System.out.print("Result: ");
		App func = new App();
		System.out.print(func.add(a, b));
		sc.close();
	}

}