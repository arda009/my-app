package com.mycompany.app;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
	App funcTest = new App();

	@Test
	public void test() {
		int a =13;
		int b =9;
		assertEquals(22,funcTest.add(a, b));
	}
	
	@Test
	public void test2() {
		int a =8;
		int b =0;
		assertEquals(8,funcTest.add(a, b));
	}
}
